#ifndef MonteCarlo
#define MonteCarlo

#include "vertex.hpp"
#include "model.hpp"
#include "particle.hpp"


class MC
{
  private:
    Model* m;
    vertex pos, dir, aa, bb, cc;
    double phi, psi, cosangle, FreePath;
    double fClosest, f2Closest;
    double Transmissivity;
    double costheta1, costheta2, sintheta1, sintheta2, psiRot, phiRot;
    double a1, a2, a3, c1, c2, s1, s2;
    int iClosest, i2Closest;
    int nParticles;


    void findClosest(Particle& p);
    void performRayTrace(Particle& p);

  public:
    // Simulation data
    int* nXletHits = NULL;
    double* totalPaths = NULL;
    double* nCollisions = NULL;
    

    void runRT(Settings* s);
    void printStats();
    double getTotalPath(int part, int mat, int mode);
    int getnParticles();
    MC(Model* m);
    ~MC();
};

#endif
