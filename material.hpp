#ifndef MATERIAL
#define MATERIAL
#include <string>

using namespace std;

struct material {
  string name;
  double ct;
  double cl;
  double density;
  double young;
  double poisson;
  double lambda;
  double mu;
  double mfp;
};
#endif
