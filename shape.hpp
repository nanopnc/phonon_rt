#ifndef SHAPE
#define SHAPE
#include <iostream>
#include "vertex.hpp"
#include <vector>

struct shape {
  string name;
  string type;
  string filename;
  vertex corner = {33e33, 33e33, 33e33};
  vector<double> vertices;
  vertex center = {0, 0, 0};
  vertex dim = {0, 0, 0};
  double extrude = 0;
  vertex axis = {0, 0, 0};
  int material = -1;
  vector<double> faceSpecularity;
  int Xlet = 0; // -1 (outlet), 0 (normal), 1 (inlet)
  bool sink = false;
  vector<int> skip;
  vector<int> periodicPosition = {0, 0, 0};
  vector<int> periodicSurfaces;
};

void printShape(const shape&);
#endif
