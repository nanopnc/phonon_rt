#include "shape.hpp"
#include <iostream>

using namespace std;

void printShape(const shape& S){
  cout << "Printing out shape details:" << endl;
  cout << "  Name: " << S.name << endl;
  cout << "  Type: " << S.type << endl;
  cout << "  Corner: " << S.corner << endl;
  cout << "  Vertices: ";
  for(int i = 0; i < S.vertices.size(); i++){
    cout << S.vertices[i];
    if(i < S.vertices.size() - 1) cout << ", ";
  }
  cout << endl;
  cout << "  Center: " << S.center << endl;
  cout << "  Dim: " << S.dim << endl;
  cout << "  Extrude: " << S.extrude << endl;
  cout << "  Axis: " << S.axis << endl;
  cout << "  Material number: " << S.material << endl;
  cout << "  Face specularity: ";
  for(int i = 0; i < S.faceSpecularity.size(); i++){
    cout << S.faceSpecularity[i];
    if(i < S.faceSpecularity.size() - 1) cout << ", ";
  }
  cout << endl;
  cout << "  Xlet type: " << S.Xlet << endl;
  cout << "  Sink: " << S.sink << endl;
  cout << "  Skip faces: ";
  for(int i = 0; i < S.skip.size(); i++){
    cout << S.skip[i];
    if(i < S.skip.size() - 1) cout << ", ";
  }
  cout << endl;
  cout << "  Periodic position: ";
  for(int i = 0; i < S.periodicPosition.size(); i++){
    cout << S.periodicPosition[i];
    if(i < S.periodicPosition.size() - 1) cout << ", ";
  }
  cout << endl;
  cout << "  Periodic surfaces: ";
  for(int i = 0; i < S.periodicSurfaces.size(); i++){
    cout << S.periodicSurfaces[i];
    if(i < S.periodicSurfaces.size() - 1) cout << ", ";
  }
  cout << endl;
}
