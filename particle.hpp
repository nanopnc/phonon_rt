#ifndef PARTICLE
#define PARTICLE

#include "vertex.hpp"
#include "model.hpp"


class Particle
{
  private:
    Model *model;
    vector<double> inletAreas;
    double totalInletArea;

  public:
    vertex position;
    vertex direction;
    vertex polarization;
    vertex rotation;
    int mode;
    int periodicPosition[3] = {0, 0, 0}; // Position in periodic array
    int inletSurface;
    int initialMaterial;
    int surface;
    int collision;
    int finalXlet;
    double *totalPaths = NULL;

    Particle(Model *m);
    void init();
    void printParticle();
    int findSurface();
    void polarize();
    void rotate(vertex, double);
    void exportFile(string);
};

#endif
