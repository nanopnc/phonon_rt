#ifndef VERTEX
#define VERTEX

#include <iostream>
#include <cmath>
#include <vector>

using namespace std;

struct vertex {
	double x;
	double y;
	double z;
};

vertex operator ^ (const vertex&, const vertex&);
vertex operator - (const vertex&, const vertex&);
vertex operator + (const vertex&, const vertex&);
vertex operator * (const double&, const vertex&);

double dot(const vertex&, const vertex&);

void unit(vertex&);

ostream& operator<< (ostream& stream, const vertex& a);

vertex as_vertex(const string& s);
vertex as_vertex(const vector<double>& v);
vertex as_vertex(double x, double y, double z);

#endif
