#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Apr  8 15:26:16 2022

@author: tuaupuur
"""

import numpy as np
import matplotlib.pyplot as plt

nMat = 2
P = 0
SV = 1
SH = 2

LUT_RES = 100

def lutkey(m1, m2, n):
    return round(3*(m1 - 1)*nMat + 3*m2 + n)

def plut(m1, m2, n):
    start = LUT_RES*lutkey(m1, m2, n)
    X = np.linspace(0, np.pi/2, LUT_RES)

    if np.sum(np.isnan(B[start:(start+LUT_RES),:])) > 0:
        print('FOUND NAN!')
    plt.clf()
    plt.plot(X, B[start:(start+LUT_RES),0],'g')
    plt.plot(X, B[start:(start+LUT_RES),1],'y')
    plt.plot(X, B[start:(start+LUT_RES),2],'r')
    plt.plot(X, B[start:(start+LUT_RES),3],'b')       
    plt.xlim([0, np.pi/2])
    plt.ylim([0,1.1])
    plt.show()

def plut2(m1, m2, n):
    nres = 400
    X = np.linspace(0, np.pi/2, nres)
    Y = np.zeros(nres)
    for xi in range(0, nres):
        Y[xi] = getEnergy(n, 0, m1, m2, X[xi])
        
    plt.clf()
    plt.plot(X, Y, 'g')
    for xi in range(0, nres):
        Y[xi] = getEnergy(n, 1, m1, m2, X[xi])

    plt.plot(X, Y,'y')
    for xi in range(0, nres):
        Y[xi] = getEnergy(n, 2, m1, m2, X[xi])

    plt.plot(X,Y,'r')
    for xi in range(0, nres):
        Y[xi] = getEnergy(n, 3, m1, m2, X[xi])

    plt.plot(X,Y,'b')       
    plt.xlim([0,np.pi/2])
    plt.ylim([0,1.1])
    plt.show()
        
        

def getEnergy(inn, out, m1, m2, angle):
    LUT_dAlpha = np.pi/2/(LUT_RES-1);
    
    i = np.floor(angle/LUT_dAlpha);
    xi = np.pi/2*(i/(LUT_RES - 1));
    yi = B[lutkey(m1,m2,inn)*LUT_RES + round(i), out]
    
    return yi + (angle - xi)*(B[lutkey(m1,m2,inn)*LUT_RES + round(i) + 1, out] - yi)/LUT_dAlpha;



A = []
with open("../lut.dat") as f:
    lines = f.readlines()
    for line in lines:
        A.append(float(line))
        
B = np.reshape(np.array(A), (-1,4))
n = len(B)
X = np.linspace(0, np.pi/2, n)       
nMat = np.sqrt(B.shape[0]/LUT_RES/2)


plut(1,0,SV)
