#!/usr/bin/python3

import matplotlib.pyplot as plt
import numpy as np
import sys

times = np.fromfile('../data.dat')

plt.hist(np.log10(times), bins=300)

plt.xlim([-15, -7])
plt.ylim([0, 10000])
plt.xlabel('time [log10(s)]')
plt.ylabel('number of hits')
#fname = sys.argv[1] + sys.argv[2] + '.png'
#plt.savefig(fname, format='png')
