from mpl_toolkits.mplot3d import Axes3D
from mpl_toolkits.mplot3d.art3d import Poly3DCollection
import matplotlib.pyplot as plt
import numpy as np


geomfile = '../geom.dat'

indices = []
vertices = []

with open(geomfile) as f:
    lines = f.readlines()
    for line in lines:
        val = np.fromstring(line, dtype=float, sep=',')
        if len(val) == 1:
            indices.append(int(val[0]))
        else:
            vertices.append(val*1e7)

verts = np.split(vertices, indices[1:])

fig = plt.figure()
ax = Axes3D(fig, autoscale_on=True)

B = Poly3DCollection(verts)
B.set_facecolor((0.5,0.2,0.9,0.3))
B.set_edgecolor('k')
ax.add_collection3d(B)


trailfile = '../trail.dat'

positions = []
direction = []
surfaces = []
modes = []
polarizations = []
rotations = []

with open(trailfile) as f:
    lines = f.readlines()
    for line in lines:
        line = line.replace('(', '').replace(')', '')
        val = np.fromstring(line, dtype=float, sep=',')
        if len(val) == 14:
            positions.append(val[0:3])
            direction.append(val[3:6])
            surfaces.append(val[6])
            modes.append(val[7])
            polarizations.append(val[8:11])
            rotations.append(val[11:14])


pos = np.array(positions)*1e7
rot = np.array(rotations)*0.1

colors = ['k', 'r', 'b']

ax.plot(pos[:, 0], pos[:, 1], pos[:, 2],'.-k')

for i in range(0, len(positions) - 1):
    pt = pos[i, :]
    d1 = rot[i, :]
    ax.plot([pt[0], pt[0]+d1[0]], [pt[1], pt[1]+d1[1]], [pt[2], pt[2]+d1[2]], 
            colors[round(modes[i])],linewidth=2)

axmax = np.max(vertices)
ax.set_xlim((0, axmax))
ax.set_ylim((0, axmax))
ax.set_zlim((0, axmax))
plt.show()
