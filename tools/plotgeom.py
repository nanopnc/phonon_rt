from mpl_toolkits.mplot3d import Axes3D
from mpl_toolkits.mplot3d.art3d import Poly3DCollection
import matplotlib.pyplot as plt
import numpy as np

geomfile = '../geom.dat'

indices = []
vertices = []

with open(geomfile) as f:
    lines = f.readlines()
    for line in lines:
        val = np.fromstring(line, dtype=float, sep=',')
        if len(val) == 1:
            indices.append(int(val[0]))
        else:
            vertices.append(val*1e5)

verts = np.split(vertices, indices[1:])

X, Y, Z = zip(*vertices)
xmin = min(X)
ymin = min(Y)
xmax = max(X)
ymax = max(Y)


fig = plt.figure()
ax = Axes3D(fig, autoscale_on=True)
B = Poly3DCollection(verts)
B.set_facecolor((0.5,0.2,0.9,0.5))
B.set_edgecolor('k')
ax.add_collection3d(B)
ax.set_xbound(xmin, xmax)
ax.set_ybound(ymin, ymax)
plt.show()
