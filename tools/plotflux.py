#!/usr/bin/python3

import matplotlib.pyplot as plt
import numpy as np
import sys


times = np.fromfile('../data.dat')
times_sorted = np.sort(times)
mmin = times_sorted[0]
mmax = times_sorted[round(0.9*times.size)]

flux_res = 400
dt = mmax/flux_res

def ker(t, dt):
    K = np.zeros(len(times))
    
    ind = np.argmax(times_sorted > t)
    ind2 = np.argmax(times_sorted > t + dt)             
    
    return ind2 - ind
    

t = 0
flux = np.zeros(flux_res)
tt = np.logspace(mmin, mmax, flux_res)
for i in range(0, flux_res - 1):
    dt = np.log(tt[i + 1]) - np.log(tt[i])
    flux[i] = ker(np.log(tt[i]), dt)/dt


plt.xlabel("time [s]")
plt.ylabel("Phonon rate [1/s]")
plt.plot(np.log(tt), flux)
plt.show()