OBJ = phononrt.o model.o particle.o mc.o settings.o vertex.o
DEPS = vertex.hpp model.hpp particle.hpp mc.hpp pcg32.h settings.hpp shape.hpp material.hpp keyvalue.hpp
HEADER = ../pcg32/pcg32.h 
CFLAGS = -I -std=c++11 -w -I../pcg32
OPT = -O3
MPIWRAPPER = mpiCC

%.o: %.cpp $(DEPS) $(HEADER)
	$(MPIWRAPPER) $(OPT) -std=c++11 -c -o $@ $<

phononrt: phononrt.cpp model.cpp particle.cpp mc.cpp vertex.cpp settings.cpp shape.cpp 
	$(MPIWRAPPER) $(OPT) -std=c++11 phononrt.cpp model.cpp particle.cpp vertex.cpp mc.cpp settings.cpp shape.cpp -o phononrt $(CFLAGS)

