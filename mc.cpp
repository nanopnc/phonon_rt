#include <iostream>
#include <cmath>
#include "mc.hpp"
#include "pcg32.h"
#define EPS 1e-15

//#define DEBUG

extern pcg32 rng;

using namespace std;

MC::MC(Model* model) {
  m = model;
  nXletHits = new int[m->nXlets];
  for(int i = 0;i < m->nXlets; i++)
    nXletHits[i] = 0;
};

void MC::findClosest(Particle& p) {
  fClosest = 9e9;
  iClosest = 0;
  double dist;
  double cosangle;
  vertex intersect;

  for(int i = 0; i < m->nSurfaces; i++) {
    cosangle = dot(m->surfaceNormals[i], p.direction);

    // Flip face normal toward current position
    if(cosangle > 0.0) {
      m->surfaceNormals[i] = -1.0*m->surfaceNormals[i];
      cosangle = -cosangle;
    }

    if(i != p.surface) {
      int vi = m->vStartIndex[i];
      double pos2face = -dot(p.position - m->surfacePoints[vi],
        m->surfaceNormals[i]);
      dist = pos2face/cosangle; //cosangle can be equal to 0 rarely

      // Only accept faces that are far away from the current position
      if(dist > EPS) {
        intersect = p.position + dist*p.direction;

        // Skip update if intersection point is not inside the face.
        if(!(m->isInside(intersect, i)))
          continue;

        // Check if this face is closer than the previous closest
        if(!(dist > fClosest)) {
          f2Closest = fClosest;
          i2Closest = iClosest;
          fClosest = dist;
          iClosest = i;
          // No significant speed loss because of the following
        } else if (!(dist > f2Closest)) {
          f2Closest = dist;
          i2Closest = i;
        }
      }
    }
  }
}

void MC::performRayTrace(Particle& p) {
  // Assumes that material number for Void is 0
  int oldMat = p.initialMaterial;
  int nextMat;
  bool transmission = false;
  double fDist = 0.0;
  double acostheta, twopiphi;

#ifdef DEBUG
  p.printParticle();
#endif

  if(m->recordTrail) {
      p.exportFile(m->trailFile);
  }

  while(!transmission)
  {
    fClosest = 33e33;
    f2Closest = 33e33;
    iClosest = -1;
    i2Closest = -1;

    // Stores the results into private attributes fClosest and iClosest
    findClosest(p);
    if(fClosest == 9e9) {
      cout << "Error: lost particle!" << endl;
      p.printParticle();
      cout << "Face normal: " << m->surfaceNormals[p.surface] << endl;
      break;
    }

    if(m->MFP[oldMat] == 33e33)
      FreePath = 33e33;
    else
      // Cut too short Freepaths: log(0.999) -> 0.001 relative Freepath length
      FreePath = -m->MFP[oldMat]*log(0.999*rng.nextDouble());

    // Cut bulk scattering too close to the walls
    if(FreePath < 0.99999*fClosest) {
      // Bulk scattering event
      nextMat = oldMat;
      p.collision += 1;

      // Move particle to the scattering site
      fDist = FreePath;
      p.totalPaths[(oldMat - 1)*3 + p.mode] += fDist;
      p.position = p.position + fDist*p.direction;
      p.surface = -1;

      // New random direction
      acostheta = acos(1.0 - 2.0*rng.nextDouble());
      twopiphi = 2.0*M_PI*rng.nextDouble();
      p.direction.x = sin(acostheta)*cos(twopiphi);
      p.direction.y = sin(acostheta)*sin(twopiphi);
      p.direction.z = -cos(acostheta);

    } else {// Boundary scattering event

      // Move particle to the boundary
      fDist = fClosest;
      p.totalPaths[(oldMat - 1)*3 + p.mode] += fDist;
      p.position = p.position + fClosest*p.direction;
      p.surface = iClosest;

      // Periodic or normal boundary
      if(m->periodicSurfaces != NULL){
        int dst = m->periodicSurfaces[iClosest];
        if(dst != 0) {
          int src = p.surface;
          int j = m->periodicDirections[dst];
          if(p.periodicPosition[j] == 0)
            dst = src;
          else
            dst -= 1; // -1 to get real face number
          // Assumes identical surface normals and face shapes
          vertex transformation =
            m->surfacePoints[m->vStartIndex[dst]]
            - m->surfacePoints[m->vStartIndex[src]];

          // Move particle to the periodic boundary
          p.position = p.position + transformation;
          p.surface = dst;

          p.periodicPosition[j] += (dst == src) + (dst < src) - (src < dst);
        }
      } else {// Normal boundary, resolve collision
        p.collision += 1;

        // Resolve next material
        // Double wall collision or single wall collision?
        if(abs(fClosest - f2Closest) < EPS) {// Double
          nextMat = abs(oldMat - m->surfaceMaterials[iClosest]
              - m->surfaceMaterials[i2Closest]);
        } else {// Single
          nextMat = oldMat - m->surfaceMaterials[iClosest];
          if(m->surfaceMaterials[iClosest] == 0) // Handle Voids separately
            nextMat = 0;
        }

        // Scattering only on material interfaces
        if(oldMat != nextMat){

          // Modify polarization with respect to surface hit
          p.polarize();

          // Specular scattering acoustic mismatch model
          if(rng.nextDouble() < m->surfaceSpecularity[iClosest]) {
            // rotation axis is on the surface, perp. to normal and p.direction
            vertex axis = p.direction^(m->surfaceNormals[p.surface]);
            unit(axis);
            p.rotation = axis;

            costheta1 = dot(p.direction, -1.0*(m->surfaceNormals[p.surface]));
            sintheta1 = sqrt(1.0 - costheta1*costheta1);

            // incident angle, critical angles taken care with energies (En = 0)
            double t0 = acos(costheta1);
            double E1 = m->getEnergyConst(p.mode, 0, oldMat, nextMat, t0);
            double E2 = m->getEnergyConst(p.mode, 1, oldMat, nextMat, t0);
            double E3 = m->getEnergyConst(p.mode, 2, oldMat, nextMat, t0);
            double E4 = m->getEnergyConst(p.mode, 3, oldMat, nextMat, t0);

            // probabilities
            double prob = rng.nextDouble();
            // Single random value enough, because prop. wave energies sum up to 1,
            // and 1st (and 2nd) critical angle converts P-refracted (SV-refracted)
            // into evanescent modes
            if(p.mode == 0) {// P-wave
              if(prob <= E1) {//reflect as P-wave
                p.rotate(axis, -t0); // same angle
                p.polarization = p.direction;
              } else if(prob <= E1+E2) {//reflect as SV-wave
                double a = asin(m->ct[oldMat] / m->cl[oldMat] * sintheta1);
                p.mode = 1;
                p.rotate(axis, -a);
                p.polarization = p.direction^axis;
              } else if(prob <= E1+E2+E4) {//refract as SV-wave
                double a = asin(m->ct[nextMat] / m->cl[oldMat] * sintheta1);
                oldMat = nextMat;
                p.mode = 1;
                p.rotate(axis, M_PI + a);
                p.polarization = p.direction^axis;
              } else {//refract as P-wave
                double a = asin(m->cl[nextMat] / m->cl[oldMat] * sintheta1);
                oldMat = nextMat;
                p.rotate(axis, M_PI + a);
                p.polarization = p.direction;
              }
            } else if(p.mode == 1) {// SV wave
              if(prob <= E1) {//reflect as P-wave
                double a = asin(m->cl[oldMat] / m->ct[oldMat] * sintheta1);
                p.mode = 0;
                p.rotate(axis, -a);
                p.polarization = p.direction;
              } else if(prob <= E1+E2) {//reflect as SV-wave
                p.rotate(axis, -t0); // same angle
                p.polarization = p.direction^axis;
              } else if(prob <= E1+E2+E4) {//refract as SV-wave
                double a = asin(m->ct[nextMat] / m->ct[oldMat] * sintheta1);
                oldMat = nextMat;
                p.rotate(axis, M_PI + a);
                p.polarization = p.direction^axis;
              } else {//refract as P-wave
                double a = asin(m->cl[nextMat] / m->ct[oldMat] * sintheta1);
                oldMat = nextMat;
                p.mode = 0;
                p.rotate(axis, M_PI + a);
                p.polarization = p.direction;
              }
            } else if(p.mode == 2) { // SH-wave
              if(prob < E1) {//reflect as SH-wave
                p.rotate(axis, -t0);
              } else {//refract as SH-wave
                double a = asin(m->ct[nextMat] / m->ct[oldMat] * sintheta1);
                oldMat = nextMat;
                p.rotate(axis, M_PI + a);
              }
            }
          } else {// Diffusive scattering diffusive mismatch model
            Transmissivity =
              m->sigma[nextMat]/(m->sigma[oldMat] + m->sigma[nextMat]);

            if(rng.nextDouble() < Transmissivity) {
              oldMat = nextMat;
              m->surfaceNormals[iClosest] = (-1.0)*m->surfaceNormals[iClosest];
            }
            phi = asin(sqrt(rng.nextDouble()));
            psi = 2*M_PI*rng.nextDouble();

            a1 = sin(phi)*cos(psi);
            a2 = sin(phi)*sin(psi);
            a3 = cos(phi);

            vertex normal = m->surfaceNormals[iClosest];
            phiRot = atan2(normal.y, normal.x);
            psiRot = acos(normal.z);

            c1 = cos(psiRot);
            c2 = cos(phiRot);
            s1 = sin(psiRot);
            s2 = sin(phiRot);

            // Rotation matrix multiplication element-wise
            p.direction.x = c1*c2*a1 - s2*a2 + s1*c2*a3;
            p.direction.y = c1*s2*a1 + c2*a2 + s1*s2*a3;
            p.direction.z = -s1*a1 + c1*a3;

            // Randomize polarization
            double pol = rng.nextDouble();
            if(pol < 0.3333) {//P-wave
              p.polarization = p.direction;
              p.mode = 0;
            } else if(pol < 0.6666) {//SV-wave
              p.polarization =
                p.direction^(p.direction^normal);
              p.mode = 1;
            } else if(pol < 1) {//SH-wave
              p.polarization = p.direction^normal;
              p.mode = 2;
            }
          }
        }
      }
      // Check inlet/outlet hits, if != -1, then it's an Xlet
      if(m->XletFaces[iClosest] != -1) {
        shape* S = &(m->Xlets[m->XletFaces[iClosest]]);
        // Check if S is a sink and if particle hits inside of it
        if(S->sink) {
          if(m->isInsideOutlet(p.position, m->XletFaces[iClosest], iClosest)) {
            transmission = true;
            p.finalXlet = m->XletFaces[iClosest];
          }
        }
      }
    }

    if(m->recordTrail) {
        p.exportFile(m->trailFile);
    }

#ifdef DEBUG
    cout << "fDist: " << fDist << endl;
    p.printParticle();
    // Stop simulation if too many collisions
    if(p.collision == 40) {
      cout << "Stopping simulation..." << endl;
      transmission = true;
    }
#endif
  }
}

MC::~MC() {
  if(nXletHits != NULL)
    delete[] nXletHits;
  if(nCollisions != NULL)
    delete[] nCollisions;
  if(totalPaths != NULL)
    delete[] totalPaths;
}

void MC::printStats() {
  cout << endl;
  cout << "Simulation with " << nParticles << " particles..." << endl;
  cout << "Average number of collisions: " << nCollisions << endl;

  for(int i = 0; i < (m->nMaterials - 1); i++) {
    double avePath = 0;
    for(int j = 0; j < nParticles; j++)
        avePath += totalPaths[(m->nMaterials - 1)*j + i];

    cout << m->name[i] << ":\t" << "average path: " <<
      avePath/nParticles << " [m], average time: NOT WORKING" << endl;
      //avePath/nParticles/m->velocity[i] << " [s]" << endl;
  }
}

void MC::runRT(Settings* s) {
  if(s->nParticles < 0) {
    cout << "Number of times to run RayTrace must be more than 0.";
    return;
  } else
    nParticles = s->nParticles;

  for(int i = 0;i < m->nXlets; i++)
    nXletHits[i] = 0.0;

  if(nCollisions == NULL) {
    nCollisions = new double[nParticles];
  }

  //totalPaths[particle][material][mode]
  if(totalPaths == NULL) {
    totalPaths = new double[3*m->nMaterials*nParticles];
  }

  auto p = new Particle(m);
  for(int i = 0; i < nParticles; i++) {
    if(m->recordTrail & i == 0) {
      cout << "Recording particle trails: " << m->trailFile << endl;
      if(rename((m->trailFile).c_str(), ((m->trailFile) + ".bck").c_str()) == 0)
        cout << "  Renamed existing " << m->trailFile << " to "
          << m->trailFile + ".bck" << endl;
    }
    p->init();
    performRayTrace(*p);

    if(p->finalXlet == 0)
      nCollisions[i] = -(p->collision);
    else 
      nCollisions[i] = p->collision;

    for(int j = 1; j < m->nMaterials; j++)
      for(int k = 0; k < 3; k++)
        totalPaths[3*(m->nMaterials - 1)*i + 3*(j - 1) + k] =
          p->totalPaths[3*(j - 1) + k];
    if(p->finalXlet != -1)
      nXletHits[p->finalXlet] += 1;
  }
}

double MC::getTotalPath(int part, int mat, int mode) {
  if(mat == 0) {
    cout << "Error: Requested travelled path in material 0 (Void), which " <<
      "is not recorded!" << endl;
    return 0;
  }
  return totalPaths[3*part*(m->nMaterials - 1) + 3*(mat - 1) + mode];
}

int MC::getnParticles() {
  return nParticles;
}
