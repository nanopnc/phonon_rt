#ifndef SETTINGS
#define SETTINGS
#include "material.hpp"
#include "shape.hpp"
#include "keyvalue.hpp"
#include <string>

using namespace std;

class Settings {
  private:
    int nPeriodicSurfaces = 0;
    int nOutlets = 0;
    int nSurfaces = 0;
    int nVertices = 0;
    int verbose = 0;
    string ltrim(const string&);
    string rtrim(const string&);
    vector<double> as_vectorDouble(const string&);
    vector<int> as_vectorInt(const string&);
    bool as_bool(const string&);
    void printVector(vector<double>);
    void printVector(vector<int>);
    int precedence(char op);
    double applyOp(double a, double b, char op);
    double evaluateExpression(const string& expression);

  public:
    int nParticles;
    string filename = "";
    string dataFile = "";
    string geomFile = "";
    string trailFile = "";
    string collisionFile = "";
    string lutFile = "";
    vector<material> materials;
    vector<shape> geometry;
    vector<keyvalue> variables;
    Settings(int, char**);
    void printSettings();
    int getTotalPeriodicSurfaces();
    int getTotalSurfaces();
    int getTotalMaterials();
    int getTotalVertices();
    int getTotalOutlets();
    bool is_verbose();
    //double readVariable(const string&);
};



#endif
