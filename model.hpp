#ifndef MODEL
#define MODEL

#include "vertex.hpp"
#include "settings.hpp"
#include "material.hpp"
#include "shape.hpp"

class Model {
  private:
    int currentSurface = -1;
    int currentMaterial = -1;
    int currentVertex = -1;
    void init(int n, int m, int v);
    vertex crossp[200];
    vector<double(*)[4]> LUTs;
    double LUT_dAlpha;
    int LUTkey(int m1, int m2, int in);
  public:
    // Geometry
    int* nVertices;
    int* vStartIndex;
    int nVerticesOutlet[10];
    int vStartIndexOutlet[10];
    vertex* surfaceNormals;
    vertex* surfacePoints;
    vertex* surfacePointsOutlet;
    vector<vector<vertex>> polys;
    double* surfaceSpecularity;
    int* surfaceMaterials;
    int* periodicSurfaces = NULL;
    int* periodicDirections = NULL;
    int* XletFaces = NULL;
    shape Xlets[10];
    vector<vector<vertex>> inletPolys;

    // Materials
    double* cl;
    double* ct;
    double* MFP;
    double* totalPath;
    double* sigma;
    double* density;
    double* mu;
    string* name;

    int nPeriodicSurfaces;
    int nPeriods;
    int nSurfaces;
    int nMaterials;
    int totalVertices;
    int totalVerticesOutlet;
    int nXlets;
    int nOutlets;
    int nInlets;

    bool geometrySet;
    bool recordTrail = false;
    string trailFile;

    Model(Settings* s);
    ~Model();

    void setMaterials(Settings*);
    void setGeometry(Settings*);

    void createLUTSolid(int, int, int);
    void createLUTVoid(int, int);
    double getEnergyLinear(int, int, int, int, double);
    double getEnergyConst(int, int, int, int, double);
    void exportLUT(string);
    void fixXlet(shape&);
    void printModel();
    void printSurface(int);
    void checkOrientation(int);
    void createInlet(const shape*);
    void createOutlet(const shape*);
    void createTriangles(const shape*);
    void createBox(const shape*);
    void createCylinder6(const shape*);
    void createCylinder8(const shape*);
    void createCylinder12(const shape*);
    void createPrism(const shape*);
    void addFace(int, int);
    void addAll(int);
    void setSpecularity(double);
    void addMaterial(material);
    void exportModel(string);

    double getArea(vector<vertex>&);
    bool isInside(const vertex&, int);
    bool isInsideOutlet(const vertex&, int, int);

};

#endif
