#include <iostream>
#include <chrono>
#include <time.h>
#include "vertex.hpp"
#include "model.hpp"
#include "particle.hpp"
#include "mc.hpp"
#include <mpi.h>
#include <vector>
#include <fstream>
#include "pcg32.h"

//#define DEBUG

using namespace std;
using namespace std::chrono;

pcg32 rng;

int main(int argc, char** argv){

  // Initialize parallel communication
  MPI_Init(&argc, &argv);

  int world_size;
  MPI_Comm_size(MPI_COMM_WORLD, &world_size);

  int world_rank;
  MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);

  // Initialize rng for each process
  //rng.seed(42u, (unsigned int)atoi(argv[2]) + (unsigned int)world_rank);
  rng.seed(42u, (unsigned int)time(NULL) + (unsigned int)world_rank);

  if(world_rank == 0)
    cout << "Building Model for each process..." << endl;
#ifdef DEBUG
  cout << "Initializing Settings..." << endl;
#endif
  Settings* s = new Settings(argc, argv);
  if(world_rank == 0)
    if(s->is_verbose())
      s->printSettings();
#ifdef DEBUG
  cout << "Initializing Model..." << endl;
#endif
  Model* m = new Model(s);
#ifdef DEBUG
  cout << "Exporting lookup tables..." << endl;
#endif
  if(world_rank == 0 && !(s->lutFile.empty())){
    m->exportLUT("lut.dat");
  }
#ifdef DEBUG
  cout << "Exporting Model..." << endl;
#endif
  if(world_rank == 0 && !(s->geomFile.empty())){
    m->exportModel(s->geomFile);
  }
#ifdef DEBUG
  cout << "Printing Model..." << endl;
#endif
  if(world_rank == 0)
    if(s->is_verbose())
      m->printModel();

#ifdef DEBUG
  cout << "Initializing MC..." << endl;
#endif
  auto simulation = new MC(m);

  if(world_rank == 0)
    cout << "Simulation run..." << endl;
  auto start = high_resolution_clock::now();
  simulation->runRT(s);

  MPI_Barrier(MPI_COMM_WORLD);
  if(world_rank == 0)
    cout << "Simulation completed." << endl;
  auto stop = high_resolution_clock::now();
  auto dur = duration_cast<milliseconds>(stop - start);

  double* Collisions;
  double* phononPaths;
  double* phononTimes;
  int nParticles = simulation->getnParticles();
  int nMaterials = m->nMaterials;
  int* XletHits;

  if(world_rank == 0){
    Collisions = new double[world_size*nParticles];
    phononPaths = new double[world_size*nParticles*(nMaterials - 1)*3];
    phononTimes = new double[world_size*nParticles];
    for(int i = 0;i < world_size*nParticles; i++)
      phononTimes[i] = 0;
    XletHits = new int[world_size*m->nXlets];
  } else { // In some systems MPI_Gather crashes if these are not allocated.
    Collisions = new double[world_size*nParticles];
    phononPaths = new double[world_size];
    phononTimes = new double[world_size];
    XletHits = new int[world_size];
  }

  // Gather collisions from all processes
  MPI_Gather(simulation->nCollisions, nParticles, MPI_DOUBLE, Collisions, nParticles, MPI_DOUBLE, 0,
    MPI_COMM_WORLD);

  // Gather average path and travel time in each material
  MPI_Gather(simulation->totalPaths,
    3*nParticles*(nMaterials - 1), MPI_DOUBLE, phononPaths,
    3*nParticles*(nMaterials - 1),
    MPI_DOUBLE, 0, MPI_COMM_WORLD);

  // Gather Xlet data from each process
  MPI_Gather(simulation->nXletHits, m->nXlets, MPI_INT, XletHits, m->nXlets,
    MPI_INT, 0, MPI_COMM_WORLD);

  if(world_rank == 0){
    vector<double> averagePaths;
    vector<double> aveTimesP;
    vector<double> aveTimesSV;
    vector<double> aveTimesSH;
    vector<double> aveTransmission;

    double globalAveCol = 0;
      for(int i = 0; i < world_size*nParticles; i++)
        globalAveCol = globalAveCol + abs(Collisions[i]);
      globalAveCol /= world_size*nParticles;

    // Collision particle data
    if(!(s->collisionFile.empty())){
      ofstream file;
      file.open(s->collisionFile, ios::out | ios::binary);
      if(file.is_open()){
        file.write((char*)Collisions, world_size*nParticles*sizeof(double));
      }
      file.close();
    }

    for(int j = 0; j < world_size*nParticles; j++)
      for(int i = 1; i < nMaterials; i++){
        phononTimes[j] +=
          phononPaths[j*(nMaterials - 1)*3 + (i - 1)*3 + 0]/s->materials[i].cl +
          phononPaths[j*(nMaterials - 1)*3 + (i - 1)*3 + 1]/s->materials[i].ct +
          phononPaths[j*(nMaterials - 1)*3 + (i - 1)*3 + 2]/s->materials[i].ct;
      }

    for(int i = 1; i < nMaterials; i++){
      double globalAvePath = 0;
      double pathP = 0;
      double pathSV = 0;
      double pathSH = 0;

      for(int j = 0; j < world_size*nParticles; j++){
        pathP += phononPaths[j*(nMaterials - 1)*3 + (i - 1)*3 + 0];
        pathSV += phononPaths[j*(nMaterials - 1)*3 + (i - 1)*3 + 1];
        pathSH += phononPaths[j*(nMaterials - 1)*3 + (i - 1)*3 + 2];
      }
      globalAvePath = (pathP + pathSV + pathSH)/(world_size*nParticles);

      averagePaths.push_back(globalAvePath);
      aveTimesP.push_back(pathP/s->materials[i].cl/(world_size*nParticles));
      aveTimesSV.push_back(pathSV/s->materials[i].ct/(world_size*nParticles));
      aveTimesSH.push_back(pathSH/s->materials[i].ct/(world_size*nParticles));
    }

    // Go through the transmission data from Xlets and average it
    for(int i = 0;i < m->nXlets; i++){
      aveTransmission.push_back(0);
      for(int j = 0;j < world_size; j++){
        aveTransmission[i] += (double) XletHits[j*m->nXlets + i];
      }
      aveTransmission[i] = aveTransmission[i]/(world_size*nParticles);
    }

    // Single particle data
    if(!(s->dataFile.empty())){
      ofstream file;
      file.open(s->dataFile, ios::out | ios::binary);
      if(file.is_open()){
        file.write((char*)phononTimes, world_size*nParticles*sizeof(double));
      }
      file.close();
    }

    cout << endl;
    cout << "----------------------PERFORMANCE----------------------" << endl;
    cout << "Simulation with " << simulation->getnParticles()*world_size <<
      " particles." << endl;
    cout << "Simulation took time: " << dur.count() << " milliseconds" << endl;
    double mpups = 1e-3*globalAveCol*world_size*
      simulation->getnParticles()/(dur.count());
    cout << "Average speed: " << mpups << " MPUPS" << endl;
    cout << endl;
    cout << "------------------------RESULTS------------------------" << endl;

    cout << "Average number of collisions: " << globalAveCol << endl;
    cout << "Average paths in materials:" << endl;
    for(int i = 1;i < m->nMaterials; i++){
      cout << "  " << s->materials[i].name << ": " << averagePaths[i - 1]*1e9
        << " [nm]" << endl;
    }
    cout << "Average travel times in materials:" << endl;
    for(int i = 1;i < m->nMaterials; i++){
      cout << "  " << s->materials[i].name << ": "
        << (aveTimesP[i - 1] + aveTimesSV[i - 1] + aveTimesSH[i - 1])*1e9
        << " [ns]" << endl;
    }
    cout << "Average transmission for each Xlet:" << endl;
    for(int i = 0; i < m->nXlets; i++){
      cout << "  " << (m->Xlets[i]).name << ": " << aveTransmission[i] << endl;
    }
  }

  delete Collisions;
  delete phononPaths;
  delete phononTimes;
  delete XletHits;
  delete simulation;
  delete s;
  delete m;

  MPI_Finalize();
  return 0;
}
