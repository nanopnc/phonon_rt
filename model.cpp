#include "model.hpp"
#include <iostream>
#include <fstream>
#include <cmath>
#include <complex>
#include <algorithm>
#include "shape.hpp"

#define EPS 1e-15
#define LUT_RES 100

//#define DEBUG

using namespace std;

Model::Model(Settings* s){
  init(s->getTotalSurfaces(), s->getTotalMaterials(), s->getTotalVertices());
  setMaterials(s);
  setGeometry(s);
  if(s->trailFile != ""){
    recordTrail = true;
    trailFile = s->trailFile;
  }

  LUT_dAlpha = M_PI/2/(LUT_RES - 1);

  LUTs.resize(LUTkey(nMaterials - 1, nMaterials - 1, 3));
  for(int i = 1; i < nMaterials; i++){
    for(int m = 0; m < 3; m++)
      createLUTVoid(i, m);
    for(int j = 1; j < nMaterials; j++){
      for(int m = 0; m < 3; m++)
        createLUTSolid(i, j, m);
    }
  }
}

int Model::LUTkey(int m1, int m2, int in){
  return 3*(m1 - 1)*nMaterials + 3*m2 + in;
}

void Model::setMaterials(Settings* s){
  for(int i = 0; i < nMaterials; i++)
    addMaterial(s->materials[i]);
}

void Model::setGeometry(Settings* s){
  if(geometrySet){
    cout << "ERROR: Geometry is already set!" << endl;
    return;
  }
  for(int i = 0; i < s->geometry.size(); i++) {
    shape a = s->geometry[i];
    if(a.type == "stl")
      createTriangles(&a);
    else if(a.type == "box")
      createBox(&a);
    else if(a.type == "cylinder6")
      createCylinder6(&a);
    else if(a.type == "cylinder8")
      createCylinder8(&a);
    else if(a.type == "cylinder12")
      createCylinder12(&a);
    else if(a.type == "prism")
      createPrism(&a);
    else if(a.type == "inlet" | a.type == "outlet") {
      if(nXlets >= 10) {
        cout << "ERROR: Maximum number of Xlets reached (10)!" << endl;
        continue;
      }
      if(a.vertices.size() == 0 & a.corner.x == 33e33 & a.corner.y == 33e33 &
        a.corner.z == 33e33) {
        cout << "ERROR: neither vertices nor corner are defined " <<
          "for " << a.name << "!" << endl;
        continue;
      }

      fixXlet(a);

      Xlets[nXlets] = a;
      nXlets++;
      if(a.type == "inlet") {
        //nVerticesInlet[nInlets] = (int)a.vertices.size()/3 + 1;
        nInlets++;
      }
      if(a.sink | a.type == "outlet") {
        nVerticesOutlet[nOutlets] = (int)a.vertices.size()/3 + 1;
        nOutlets++;
      }
      continue; // Xlet face is not part of the geometry
    } else {
      cout << "ERROR: Unknown geometry shape: " << a.type << endl;
      return;
    }

    for(int j = 0; j < polys.size(); j++){
      // Check if face j is in the skip list
      auto it = find(a.skip.begin(), a.skip.end(), j);
      // if not found, then add face
      if(it == a.skip.end()){
        addFace(j, a.material);
        if(a.faceSpecularity.size() > j)
          setSpecularity(a.faceSpecularity[j]);
        else
          setSpecularity(a.faceSpecularity[0]);
      }
    }
  }

  // Go through Xlets, create inlets and outlets
  if(nXlets == 0) {
    cout << "ERROR: Geometry does not have inlets and outlets!" << endl;
    return;
  }

  // Calculate outlet vertices for memory allocation
  totalVerticesOutlet = 0;
  for(int i = 0; i < nOutlets; i++){
    totalVerticesOutlet += nVerticesOutlet[i];
  }
#ifdef DEBUG
  cout << "nXlets: " << nXlets << " with " << nInlets << " inlets and " <<
    nOutlets << " outlets." << endl;
#endif
  surfacePointsOutlet = new vertex[totalVerticesOutlet];
  for(int i = 0; i < nXlets; i++) {
    if(Xlets[i].type == "inlet")
      createInlet(&(Xlets[i]));
    if(Xlets[i].sink){
      vStartIndexOutlet[i] = currentVertex + 1;
      createOutlet(&(Xlets[i]));
    }
  }

  // Find faces overlapping with Xlets
  XletFaces = new int[nSurfaces];
  for(int i = 0; i < nSurfaces; i++)
    XletFaces[i] = -1;
  for(int i = 0; i < nXlets; i++){
    shape* S = &Xlets[i];

    // Assume Xlet is inside the face if its center is
    for(int j = 0; j < nSurfaces; j++){
      if(isInside(S->center, j)){
        // XletFaces[j] = -1 if no Xlet on the face j
        // and XletFaces[j] = i if Xlet i is on the face j.
        XletFaces[j] = i;
        continue;
      }
    }
  }
  geometrySet = true;
}

void Model::init(int n, int m, int v){
  geometrySet = false;
  nSurfaces = n;
  nMaterials = m;
  totalVertices = v;
  nXlets = 0;
  nInlets = 0;
  nOutlets = 0;
#ifdef DEBUG
  cout << "Model init values: "
    << nSurfaces << ", " << nMaterials << ", " << totalVertices << endl;
#endif

  nVertices = new int[nSurfaces];
  vStartIndex = new int[nSurfaces];
  // Extra vertex per surface
  surfacePoints = new vertex[totalVertices+nSurfaces];
  surfaceNormals = new vertex[nSurfaces];
  surfaceSpecularity = new double[nSurfaces];
  surfaceMaterials = new int[nSurfaces];

  MFP = new double[nMaterials];
  totalPath = new double[nMaterials];
  for(int i = 0;i < nMaterials;i++) totalPath[i] = 0;
  sigma = new double[nMaterials];
  density = new double[nMaterials];
  cl = new double[nMaterials];
  ct = new double[nMaterials];
  mu = new double[nMaterials];
  name = new string[nMaterials];
}

void Model::fixXlet(shape& S) {
  int length = S.vertices.size();
  // Vertices data overrides center, corner and dimension inputs
  if(length > 0){
    vertex corner = {S.vertices[0], S.vertices[1], S.vertices[2]};
    vertex center = corner;
    vertex dim = {0, 0, 0};

    for(int i = 1; i < length/3; i++) {
      vertex c2 =
        {S.vertices[3*i], S.vertices[3*i + 1], S.vertices[3*i + 2]};
      center = (1.0/(i + 1.0))*((double)i*center + c2);
      dim.x = max(dim.x, abs(corner.x - c2.x));
      dim.y = max(dim.y, abs(corner.y - c2.y));
      dim.z = max(dim.z, abs(corner.z - c2.z));
    }
    S.center = center;
    S.corner = corner;
    S.dim = dim;

  } else { // Calculate vertices from corner and dimension data
    // Assume rectangle, TODO: make vertices vector a vector<vertex> instead
    // already in Settings

    S.center = S.corner + 0.5*S.dim;
    double w = S.dim.x;
    double d = S.dim.y;
    double h = S.dim.z;
    double cx = S.corner.x;
    double cy = S.corner.y;
    double cz = S.corner.z;
    S.vertices.push_back(cx);
    S.vertices.push_back(cy);
    S.vertices.push_back(cz);
    if(w == 0) {
      S.vertices.push_back(cx);
      S.vertices.push_back(cy + d);
      S.vertices.push_back(cz);

      S.vertices.push_back(cx);
      S.vertices.push_back(cy + d);
      S.vertices.push_back(cz + h);

      S.vertices.push_back(cx);
      S.vertices.push_back(cy);
      S.vertices.push_back(cz + h);
    } else if(d == 0) {
      S.vertices.push_back(cx);
      S.vertices.push_back(cy);
      S.vertices.push_back(cz + h);

      S.vertices.push_back(cx + w);
      S.vertices.push_back(cy);
      S.vertices.push_back(cz + h);

      S.vertices.push_back(cx + w);
      S.vertices.push_back(cy);
      S.vertices.push_back(cz);
    } else if(h == 0) {
      S.vertices.push_back(cx);
      S.vertices.push_back(cy + d);
      S.vertices.push_back(cz);

      S.vertices.push_back(cx + w);
      S.vertices.push_back(cy + d);
      S.vertices.push_back(cz);

      S.vertices.push_back(cx + w);
      S.vertices.push_back(cy);
      S.vertices.push_back(cz);
    }
  }
}

Model::~Model() {

  for(int i = 0; i < LUTs.size(); i++){
    delete[] LUTs[i];
  }

  delete[] vStartIndex;
  delete[] nVertices;
  delete[] surfacePoints;
  if(surfacePointsOutlet != NULL)
    delete[] surfacePointsOutlet;
  delete[] surfaceNormals;
  delete[] surfaceSpecularity;
  delete[] surfaceMaterials;
  if(periodicSurfaces != NULL)
    delete[] periodicSurfaces;
  if(periodicDirections != NULL)
    delete[] periodicDirections;

  delete[] cl;
  delete[] ct;
  delete[] mu;
  delete[] MFP;
    delete[] sigma;
  delete[] density;
  delete[] name;
  delete[] totalPath;
  delete[] XletFaces;
}

void Model::addMaterial(material mat)
{
  currentMaterial++;
  name[currentMaterial] = mat.name;
  cl[currentMaterial] = mat.cl;
  ct[currentMaterial] = mat.ct;
  mu[currentMaterial] = mat.mu;
  density[currentMaterial] = mat.density;
  sigma[currentMaterial] = 1/(mat.cl*mat.cl) + 2/(mat.ct*mat.ct);
  MFP[currentMaterial] = mat.mfp;
}

void Model::setSpecularity(double spec){
  surfaceSpecularity[currentSurface] = spec;
}

void Model::printSurface(int n) {
  cout << "Face " << n << ": " << endl;
  for(int i = 0; i < nVertices[n]; i++){
    cout << "      v" << i << ": " << surfacePoints[vStartIndex[n] + i] << endl;
  }
  cout << "      Normal: " << surfaceNormals[n] << endl;
  cout << endl;
}

void Model::printModel() {
  cout << "Model contents:" << endl;
  for(int i = 0; i < nSurfaces; i++) {
    printSurface(i);
    cout << "    Surface normal:" << endl;
    cout << "      " << surfaceNormals[i] << endl;
    cout << "    Surface specularity:" << endl;
    cout << "      " << surfaceSpecularity[i] << endl;
    cout << "    Surface materials:" << endl;
    cout << "      " << surfaceMaterials[i] << endl;
  }
#ifdef DEBUG
  for(int i = 0;i < nXlets; i++){
    printShape(Xlets[i]);
    cout << "  nVerticesOutlet: " << nVerticesOutlet[i] << endl;
    cout << "  vStartIndexOutlet: " << vStartIndexOutlet[i] << endl;
  }
#endif
}

void Model::checkOrientation(int n) {
  vertex a;
  vertex b;
  vertex c;
  int vi = vStartIndex[n];
  a = surfacePoints[vi + 0] - surfacePoints[vi + 1];
  b = surfacePoints[vi + 0] - surfacePoints[vi + 2];
  c = a^b;
  surfaceNormals[n] = (1.0/sqrt(dot(c, c)))*c;
}

void Model::addFace(int n, int material) {
  currentSurface++;
  nVertices[currentSurface] = polys[n].size();

  if(currentSurface == 0)
    vStartIndex[currentSurface] = 0;
  else
    vStartIndex[currentSurface] =
      vStartIndex[currentSurface - 1] + nVertices[currentSurface - 1];
#ifdef DEBUG
  cout << "Adding face to slot " << vStartIndex[currentSurface]
    << " with " << nVertices[currentSurface] << " vertices." << endl;
#endif
  for(int i = 0; i < nVertices[currentSurface]; i++)
    surfacePoints[vStartIndex[currentSurface] + i] = polys[n][i];

  checkOrientation(currentSurface);
  surfaceMaterials[currentSurface] = material;

#ifdef DEBUG
  printSurface(currentSurface);
#endif
}

void Model::addAll(int material) {
  for(int i = 0; i < polys.size(); i++)
    addFace(i, material);
}

void Model::exportModel(string fname) {
  ofstream file;
  file.open(fname, ios::out);
  if(file.is_open()){
    for(int n = 0;n < nSurfaces;n++)
      file << vStartIndex[n] << "\n";
    for(int n = 0;n < nSurfaces;n++){
      int vi = vStartIndex[n];
      for(int i = 0;i < nVertices[n];i++){
        vertex v = surfacePoints[vi + i];
        file << v.x << ", " << v.y << ", " << v.z << "\n";
      }
    }
  }
  file.close();
}

double Model::getArea(vector<vertex>& a) {
  // Convex coplanar polygon area
  vertex sum = {0, 0, 0};
  for(int i = 1;i < a.size();i++){
    sum = sum + (a[i]^a[i-1]);
  }
  return 0.5*sqrt(dot(sum, sum));
}

bool Model::isInside(const vertex& pt, int surfaceNum) {
  int nv = nVertices[surfaceNum];
  int vi = vStartIndex[surfaceNum];

  // Distance is larger than zero, exit
  if(abs(dot(pt - surfacePoints[vi], surfaceNormals[surfaceNum])) > EPS)
    return false;

  // Array of vertexes, define crossp[0] first for the 'if' clause in loop
  crossp[0] =
    (surfacePoints[vi + 1] - surfacePoints[vi])^
    (pt - surfacePoints[vi]);
  for(int j = 1; j < (nv - 1); j++){
    crossp[j] =
      (surfacePoints[vi + j + 1] - surfacePoints[vi + j])^
      (pt - surfacePoints[vi + j]);

    // If any dot product of cross products is negative, point is not inside
    if(dot(crossp[j - 1], crossp[j%(nv - 1)]) < 0.0)
      return false;
  }
  return true;
}

bool Model::isInsideOutlet(const vertex& pt, int XletNum, int surfaceNum) {
  // Outlet is overlapping a geometry face
  int vi = vStartIndex[surfaceNum];
  // Distance is larger than zero, exit
  if(abs(dot(pt - surfacePoints[vi], surfaceNormals[surfaceNum])) > EPS)
    return false;
  vi = vStartIndexOutlet[XletNum];
  int nv = nVerticesOutlet[XletNum];

  // Array of vertices, define crossp[0] first for the 'if' clause in loop
  crossp[0] =
    (surfacePointsOutlet[vi + 1] - surfacePointsOutlet[vi])^
    (pt - surfacePointsOutlet[vi]);

  for(int j = 1; j < (nv - 1); j++){
    crossp[j] = (surfacePointsOutlet[vi + j + 1] - surfacePointsOutlet[vi + j])^
        (pt - surfacePointsOutlet[vi + j]);
    // If any dot product of cross products is negative, point is not inside
    if(dot(crossp[j - 1], crossp[j%(nv - 1)]) < 0.0)
      return false;
  }
  return true;
}

void Model::createOutlet(const shape* S) {
#ifdef DEBUG
  cout << "Creating outlet " << S->name << "." << endl;
#endif

  int length = S->vertices.size();
  if(length > 0){
    for(int i = 0; i < length/3; i++){
      currentVertex++;
      vertex v = {S->vertices[3*i], S->vertices[3*i + 1], S->vertices[3*i + 2]};
      surfacePointsOutlet[currentVertex] = v;
    }
    // Add the first vertex again
    currentVertex++;
    surfacePointsOutlet[currentVertex] =
      {S->vertices[0], S->vertices[1], S->vertices[2]};
  } else {
    double cx = (S->corner).x;
    double cy = (S->corner).y;
    double cz = (S->corner).z;
    double w = (S->dim).x;
    double d = (S->dim).y;
    double h = (S->dim).z;

    if(!((w == 0) | (d == 0) | (h == 0))){
      cout << "Error: One dimension component must be zero for "
        << S->name << endl;
      return;
    }

    if(w == 0){
      currentVertex++;
      surfacePointsOutlet[currentVertex] = {cx, cy, cz};
      currentVertex++;
      surfacePointsOutlet[currentVertex] = {cx, cy + d, cz};
      currentVertex++;
      surfacePointsOutlet[currentVertex] = {cx, cy + d, cz + h};
      currentVertex++;
      surfacePointsOutlet[currentVertex] = {cx, cy, cz + h};
      currentVertex++;
      surfacePointsOutlet[currentVertex] = {cx, cy, cz};
    }
    else if(d == 0){
      currentVertex++;
      surfacePointsOutlet[currentVertex] = {cx, cy, cz};
      currentVertex++;
      surfacePointsOutlet[currentVertex] = {cx + w, cy, cz};
      currentVertex++;
      surfacePointsOutlet[currentVertex] = {cx + w, cy, cz + h};
      currentVertex++;
      surfacePointsOutlet[currentVertex] = {cx, cy, cz + h};
      currentVertex++;
      surfacePointsOutlet[currentVertex] = {cx, cy, cz};
    }
    else if(h == 0){
      currentVertex++;
      surfacePointsOutlet[currentVertex] = {cx, cy, cz};
      currentVertex++;
      surfacePointsOutlet[currentVertex] = {cx + w, cy, cz};
      currentVertex++;
      surfacePointsOutlet[currentVertex] = {cx + w, cy + d, cz};
      currentVertex++;
      surfacePointsOutlet[currentVertex] = {cx, cy + d, cz};
      currentVertex++;
      surfacePointsOutlet[currentVertex] = {cx, cy, cz};
    }
  }
}

void Model::createInlet(const shape* S) {
#ifdef DEBUG
  cout << "Creating inlet " << S->name << "." << endl;
#endif
  // If defined with vertices vector
  int length = S->vertices.size();
  if(length > 0) {
    vertex v = {S->vertices[0], S->vertices[1], S->vertices[2]};
    inletPolys.push_back({v});
    for(int i = 1; i < length/3; i++)
      inletPolys.back().push_back({S->vertices[3*i], S->vertices[3*i + 1],
        S->vertices[3*i + 2]});
    inletPolys.back().push_back(v);

  } else { // or with corner + dimension vectors
    double cx = (S->corner).x;
    double cy = (S->corner).y;
    double cz = (S->corner).z;
    double w = (S->dim).x;
    double d = (S->dim).y;
    double h = (S->dim).z;

    if(!((w == 0) | (d == 0) | (h == 0))){
      cout << "ERROR: One inlet dimension component must be zero." << endl;
      return;
    }

    if(w == 0){
      inletPolys.push_back({
        {cx, cy, cz},
        {cx, cy + d, cz},
        {cx, cy + d, cz + h},
        {cx, cy, cz + h},
        {cx, cy, cz}});
    }
    else if(d == 0){
      inletPolys.push_back({
        {cx, cy, cz},
        {cx + w, cy, cz},
        {cx + w, cy, cz + h},
        {cx, cy, cz + h},
        {cx, cy, cz}});
    }
    else if(h == 0){
      inletPolys.push_back({
        {cx, cy, cz},
        {cx + w, cy, cz},
        {cx + w, cy + d, cz},
        {cx, cy + d, cz},
        {cx, cy, cz}});
    }
  }
}

void Model::createTriangles(const shape* S) {
  polys.clear();
  int nTriangles = (S->vertices).size()/9;
  for(int i = 0; i < nTriangles; i++){
    vertex v0 = {S->vertices[9*i + 0], S->vertices[9*i + 1], S->vertices[9*i + 2]};
    vertex v1 = {S->vertices[9*i + 3], S->vertices[9*i + 4], S->vertices[9*i + 5]};
    vertex v2 = {S->vertices[9*i + 6], S->vertices[9*i + 7], S->vertices[9*i + 8]};

    polys.push_back({v0, v2, v1, v0});
  }
}

void Model::createBox(const shape* S) {
  vector<vertex> v;
  int csize = (S->vertices).size();
  if(csize == 0) {
    double cx = (S->corner).x;
    double cy = (S->corner).y;
    double cz = (S->corner).z;
    double w = (S->dim).x;
    double d = (S->dim).y;
    double h = (S->dim).z;

    for(int i = 0; i < 2; i++)
      for(int j = 0; j < 4; j++) {
        int x = ((j + 1)/2)%2;
        int y = j/2;
        v.push_back({cx + x*w, cy + y*d, cz + i*h});
      }
  } else {
    vertex pt;
    vertex v0 = {S->vertices[0], S->vertices[1], S->vertices[2]};
    vertex o1 = {S->vertices[3], S->vertices[4], S->vertices[5]};
    vertex o2 = {S->vertices[6], S->vertices[7], S->vertices[8]};
    o1 = o1 - v0;
    o2 = o2 - v0;
    unit(o1);
    unit(o2);
    vertex normal = o1^o2;
    unit(normal);

    for(int i = 0; i < 2; i++)
      for(int j = 0; j < csize/3; j++) {
        pt.x = S->vertices[3*j + 0] + i*S->extrude*normal.x;
        pt.y = S->vertices[3*j + 1] + i*S->extrude*normal.y;
        pt.z = S->vertices[3*j + 2] + i*S->extrude*normal.z;
        v.push_back(pt);
      }
  }

  polys.clear();
  csize = v.size();

  // Add side faces first, then bottom and top
  for(int i = 0; i < csize/2 - 1; i++) {
    polys.push_back({v[i], v[csize/2 + i], v[csize/2 + i + 1],
      v[i + 1], v[i]});
  }
  polys.push_back({v[csize/2 - 1], v[csize - 1], v[csize/2], v[0],
    v[csize/2 - 1]});

  polys.push_back({v[0]});
  for(int i = 1; i < csize/2; i++)
    polys.back().push_back(v[i]);
  polys.back().push_back(v[0]);

  polys.push_back({v[csize/2]});
  for(int i = csize - 1; i >= csize/2; i--)
    polys.back().push_back(v[i]);
}

void Model::createCylinder6(const shape* S){

  double cx = (S->center).x;
  double cy = (S->center).y;
  double cz = (S->center).z;
  double w = (S->dim).x;
  double d = (S->dim).y;
  double h = (S->dim).z;

  // Create vector of vertexes for
  vector<vertex> v;
  vertex pt;
  for(int i = 0;i < 12;i++){
    pt = {cx + w/2*cos((double)i/3*M_PI),
          cy + d/2*sin((double)i/3*M_PI),
          cz + (i > 5)*h};
    v.push_back(pt);
  }

  polys.clear();
  //Sides
  for(int i = 0;i < 6;i++){
    polys.push_back(
      {v[(1 + i)%6], v[i], v[6 + i], v[6 + (1 + i)%6], v[(1 + i)%6]});
  }
  //Bottom
  polys.push_back(
    {v[0], v[1], v[2], v[3], v[4], v[5], v[0]});

  //Top
  polys.push_back(
    {v[6], v[7], v[8], v[9], v[10], v[11], v[6]});

}

void Model::createCylinder8(const shape* S){

  double cx = (S->center).x;
  double cy = (S->center).y;
  double cz = (S->center).z;
  double w = (S->dim).x;
  double d = (S->dim).y;
  double h = (S->dim).z;

  // Create vector of vertexes for
  vector<vertex> v;
  vertex pt;
  for(int i = 0;i < 16;i++){
    pt = {cx + w/2*cos((double)i/4*M_PI),
          cy + d/2*sin((double)i/4*M_PI),
          cz + (i > 7)*h};
    v.push_back(pt);
  }

  polys.clear();
  //Sides
  for(int i = 0;i < 8;i++){
    polys.push_back(
      {v[(1 + i)%8], v[i], v[8 + i], v[8 + (1 + i)%8], v[(1 + i)%8]});
  }
  //Bottom
  polys.push_back(
    {v[0], v[1], v[2], v[3], v[4], v[5], v[6], v[7], v[0]});

  //Top
  polys.push_back(
    {v[8], v[9], v[10], v[11], v[12], v[13], v[14], v[15], v[8]});

}

void Model::createCylinder12(const shape* S){

  double cx = (S->center).x;
  double cy = (S->center).y;
  double cz = (S->center).z;
  double w = (S->dim).x;
  double d = (S->dim).y;
  double h = (S->dim).z;

  // Create vector of vertexes for
  vector<vertex> v;
  vertex pt;
  for(int i = 0;i < 24;i++){
    pt = {cx + w/2*cos((double)i/6*M_PI),
          cy + d/2*sin((double)i/6*M_PI),
          cz + (i > 11)*h};
    v.push_back(pt);
  }

  polys.clear();
  //Sides
  for(int i = 0;i < 12;i++){
    polys.push_back(
      {v[(1 + i)%12], v[i], v[12 + i], v[12 + (1 + i)%12], v[(1 + i)%12]});
  }

  // Bottom top large polys
  polys.push_back(
    {v[0], v[1], v[2], v[3], v[4], v[5], v[6], v[7], v[8],
    v[9], v[10], v[11], v[0]});
  polys.push_back(
    {v[12], v[13], v[14], v[15], v[16], v[17], v[18], v[19], v[20],
    v[21], v[22], v[23], v[12]});

}

void Model::createPrism(const shape* S) {
  vector<vertex> v;
  vertex pt;
  vertex dim = S->dim;
  int csize = (S->vertices).size();
  if(csize != 9){
    cout << "ERROR: Shape " << S->name << " has insufficient " <<
      "number of vertices (" << csize/3 << ")!" << endl;
    return;
  }

  if((S->extrude) == 0){
    cout << "ERROR: Prism must have 'extrude' parameter defined!" << endl;
    return;
  }

  vertex v0 = {S->vertices[0], S->vertices[1], S->vertices[2]};
  vertex o1 = {S->vertices[3], S->vertices[4], S->vertices[5]};
  vertex o2 = {S->vertices[6], S->vertices[7], S->vertices[8]};
  o1 = o1 - v0;
  o2 = o2 - v0;
  unit(o1);
  unit(o2);
  vertex normal = o1^o2;
  unit(normal);

  for(int j = 0; j < 2; j++){
    for(int i = 0; i < csize/3; i++){
      pt.x = S->vertices[3*i + 0] + S->extrude*j*normal.x;
      pt.y = S->vertices[3*i + 1] + S->extrude*j*normal.y;
      pt.z = S->vertices[3*i + 2] + S->extrude*j*normal.z;
      v.push_back(pt);
    }
  }
  polys.clear();
  polys.push_back({v[0], v[3], v[4], v[1], v[0]});
  polys.push_back({v[1], v[4], v[5], v[2], v[1]});
  polys.push_back({v[2], v[5], v[3], v[0], v[2]});
  polys.push_back({v[0], v[1], v[2], v[0]});
  polys.push_back({v[3], v[4], v[5], v[3]});
}

double Model::getEnergyLinear(int in, int out, int m1, int m2, double angle){
  // linear interpolation between LUT values
  auto ptr = LUTs[LUTkey(m1, m2, in)];
  int i = (int)floor(angle/LUT_dAlpha);
  double xi = M_PI/2*((double)i/(LUT_RES - 1));
  double yi = ptr[i][out];
  double yi1 = ptr[i + 1][out];
  return yi + (angle - xi)*(yi1 - yi)/LUT_dAlpha;
}

double Model::getEnergyConst(int in, int out, int m1, int m2, double angle){
  // next value in the LUT table
  auto ptr = LUTs[LUTkey(m1, m2, in)];
  int i = (int)floor(angle/LUT_dAlpha) + 1;
  return ptr[i][out];
}

void Model::createLUTVoid(int mat1, int in){
  auto ptr = new double[LUT_RES][4];
  LUTs[LUTkey(mat1, 0, in)] = ptr;
  double theta;
  // SH wave
  if(in == 2){
    for(int i = 0; i < LUT_RES; i++){
      ptr[i][0] = 1; // SH refl
      ptr[i][1] = 0; // SH refr
      ptr[i][2] = 0; // not used
      ptr[i][3] = 0; // not used
    }
  } else if(in == 0) { // P wave
    double ct1 = ct[mat1];
    double cl1 = cl[mat1];
    for(int i = 0; i < LUT_RES; i++){
      theta = (M_PI/2 - 1e-9)*((double)i/(LUT_RES-1)) + 1e-9;
      double theta2 = asin(ct1/cl1*sin(theta));
      double sin2t = sin(2*theta);
      double sin2t2 = sin(2*theta2);
      double cos2t2 = cos(2*theta2);
      double div = sin2t*sin2t2 + pow(cl1/ct1, 2)*pow(cos2t2, 2);
      double amp = (sin2t*sin2t2 - pow(cl1/ct1, 2)*pow(cos2t2, 2))/div;
      ptr[i][0] = abs(pow(amp, 2)); // P refl
      ptr[i][1] = 1 - ptr[i][0]; // SV refl
      ptr[i][2] = 0; // P refr
      ptr[i][3] = 0; // SV refr
    }
  } else if(in == 1) {// SV wave
    double ct1 = ct[mat1];
    double cl1 = cl[mat1];
    double tcritp = asin(ct1/cl1);
    for(int i = 0; i < LUT_RES; i++){
      if(theta < tcritp){
        double theta1 = asin(cl1/ct1*sin(theta));
        double sin2t = sin(2*theta);
        double sin2t1 = sin(2*theta1);
        double cos2t = cos(2*theta);
        double div = sin2t*sin2t1 + pow(cl1/ct1, 2)*pow(cos2t, 2);
        double amp = -cl1/ct1*sin(4*theta)/div;
        ptr[i][0] = abs(pow(amp, 2)); // P refl
        ptr[i][1] = 1 - ptr[i][0]; // SV refl
        ptr[i][2] = 0; // P refr
        ptr[i][3] = 0; // SV refr
      } else {
        ptr[i][0] = 0;
        ptr[i][1] = 1; // SV refl
        ptr[i][2] = 0;
        ptr[i][3] = 0;
      }
    }
  }
}

void Model::createLUTSolid(int mat1, int mat2, int in){
  auto ptr = new double[LUT_RES][4];
  LUTs[LUTkey(mat1, mat2, in)] = ptr;
  double theta;
  // SH wave
  if(in == 2){
    double mu1 = mu[mat1];
    double mu2 = mu[mat2];
    double ct1 = ct[mat1];
    double ct2 = ct[mat2];
    double sint, cost, tcrit1;

    if(ct1 < ct2)
      tcrit1 = asin(ct1/ct2);
    else
      tcrit1 = M_PI/2;

    for(int i = 0; i < LUT_RES; i++){
      theta = (M_PI/2 - 1e-9)*((double)i/(LUT_RES-1)) + 1e-9;

      if(theta < tcrit1){
        sint = sin(theta);
        cost = cos(theta);

        double amp = (mu1*cost - (ct1*mu2*sqrt(1 - pow(ct2*sint/ct1, 2)))/ct2)/
          (mu1*cost + (ct1*mu2*sqrt(1 - pow(ct2*sint/ct1, 2)))/ct2);
        ptr[i][0] = abs(pow(amp, 2)); // SH refl
        ptr[i][1] = 1 - abs(pow(amp, 2)); // SH refr
        ptr[i][2] = 0;
        ptr[i][3] = 0;
      } else { //total internal reflection
        ptr[i][0] = 1; // SH refl
        ptr[i][1] = 0;
        ptr[i][2] = 0;
        ptr[i][3] = 0;
      }
    }
  // P & SV waves
  } else {
    double tcrit1, tcrit2, tcritp;
    complex<double> l1, l2, l3, l4, m1, m2, m3, m4;
    complex<double> a, b, ap, bp;
    complex<double> div;
    double mu1 = mu[mat1];
    double mu2 = mu[mat2];
    double rho1 = density[mat1];
    double rho2 = density[mat2];
    double ct1 = ct[mat1];
    double cl1 = cl[mat1];
    double ct2 = ct[mat2];
    double cl2 = cl[mat2];
    double sint, cost;

    for(int i = 0; i < LUT_RES; i++){
      theta = (M_PI/2 - 1e-9)*((double)i/(LUT_RES-1)) + 1e-9;
      sint = sin(theta);
      cost = cos(theta);

      //Formulas below derived from Ewing (1957)
      // P wave
      if(in == 0){
        if(cl1 < cl2)
          tcrit1 = asin(cl1/cl2);
        else
          tcrit1 = M_PI/2;
        if(cl1 < ct2)
          tcrit2 = asin(cl1/ct2);
        else
          tcrit2 = M_PI/2;
        if(cl1 < ct1)
          tcritp = asin(cl1/ct1);
        else
          tcritp = M_PI/2;

        a = -cost/sint;
        b = -cl1*sqrt(complex<double>(1.0 - pow(ct1/cl1*sint, 2)))/(ct1*sint);
        ap = -cl1*sqrt(complex<double>(1.0 - pow(cl2/cl1*sint, 2)))/(cl2*sint);
        bp = -cl1*sqrt(complex<double>(1.0 - pow(ct2/cl1*sint, 2)))/(ct2*sint);
      // SV wave
      } else {
        if(ct1 < cl2)
          tcrit1 = asin(ct1/cl2);
        else
          tcrit1 = M_PI/2;
        if(ct1 < ct2)
          tcrit2 = asin(ct1/ct2);
        else
          tcrit2 = M_PI/2;
        if(ct1 < cl1)
          tcritp = asin(ct1/cl1);
        else
          tcritp = M_PI/2;

        a = -ct1*sqrt(complex<double>(1.0 - pow(cl1/ct1*sint, 2)))/(cl1*sint);
        b = -cost/sint;
        ap = -ct1*sqrt(complex<double>(1.0 - pow(cl2/ct1*sint, 2)))/(cl2*sint);
        bp = -ct1*sqrt(complex<double>(1.0 - pow(ct2/ct1*sint, 2)))/(ct2*sint);
      }
      l1 = (2.0*mu1 + mu2*(pow(bp, 2) - 1.0))/(mu1*(pow(b, 2) + 1.0));
      l2 = (mu1*(pow(b, 2) - 1.0) - mu2*(pow(bp, 2) - 1.0))/
        (b*mu1*(pow(b, 2) + 1.0));
      l3 = (ap*(2.0*mu2 + mu1*(pow(b, 2) - 1.0)))/(a*mu1*(pow(b, 2) + 1.0));
      l4 = -(2.0*ap*(mu1 - mu2))/(mu1*(pow(b, 2) + 1.0));
      m1 = (2.0*bp*(mu1 - mu2))/(mu1*(pow(b, 2) + 1.0));
      m2 = (bp*(2.0*mu2 + mu1*(pow(b, 2) - 1.0)))/(b*mu1*(pow(b, 2) + 1.0));
      m3 = -(mu1*(pow(b, 2) - 1.0) - mu2*(pow(bp, 2) - 1.0))/
        (a*mu1*(pow(b, 2) + 1.0));
      m4 = (2.0*mu1 + mu2*(pow(bp, 2) - 1.0))/(mu1*(pow(b, 2) + 1.0));

      div = (l1 + l3)*(m2 + m4) - (l2 + l4)*(m1 + m3);
      if(in == 0){ // P wave
        complex<double> amp;
        amp = ((l1 - l3)*(m2 + m4) - (l2 + l4)*(m1 - m3))/div;
        ptr[i][0] = abs(pow(amp, 2)); // P refl

        amp = 2.0*(l4*m2 - m4*l2)/div;
        ptr[i][1] = abs(cl1*sqrt(complex<double>(
          -(pow(ct1*sint, 2) - pow(cl1,2))/pow(cl1, 2)))/
          (ct1*cost)*pow(amp, 2)); // SV refl

        if(theta < tcrit1){
          amp = 2.0*(m2 + m4)/div;
          ptr[i][2] = abs(rho2/rho1*cl1*sqrt(complex<double>(
            -(pow(cl2*sint, 2) - pow(cl1, 2))/
            pow(cl1, 2)))/(cl2*cost)*pow(amp, 2)); // P refr
        } else ptr[i][2] = 0;

        if(theta < tcrit2){
          amp = -2.0*(l2 + l4)/div;
          ptr[i][3] = abs(rho2/rho1*cl1*sqrt(complex<double>(
            -(pow(ct2*sint, 2) - pow(cl1, 2))/
            pow(cl1, 2)))/(ct2*cost)*pow(amp, 2)); // SV refr
        } else ptr[i][3] = 0;
      } else { // SV wave
        complex<double> amp;
        if(theta < tcritp){
          amp = -2.0*(l1*m3 - l3*m1)/div;
          ptr[i][0] = abs(ct1*sqrt(complex<double>(
            -(pow(cl1*sint, 2) - pow(ct1,2))/pow(ct1, 2)))/
            (cl1*cost)*pow(amp, 2)); // P refl
        } else ptr[i][0] = 0;

        amp = -((l1 + l3)*(m2 - m4) - (l2 - l4)*(m1 + m3))/div;
        ptr[i][1] = abs(pow(amp, 2)); // SV refl

        if(theta < tcrit1){
          amp = -2.0*(m1 + m3)/div;
          ptr[i][2] = abs(rho2/rho1*ct1*sqrt(complex<double>(
            -(pow(cl2*sint, 2) - pow(ct1, 2))/
            pow(ct1, 2)))/(cl2*cost)*pow(amp, 2));
        } else ptr[i][2] = 0; // P refr

        if(theta < tcrit2){
          amp = 2.0*(l1 + l3)/div;
          ptr[i][3] = abs(rho2/rho1*ct1*sqrt(complex<double>(
            -(pow(ct2*sint, 2) - pow(ct1, 2))/
            pow(ct1, 2)))/(ct2*cost)*pow(amp, 2));
        } else ptr[i][3] = 0; // SV refr
      }
    }
  }
}

void Model::exportLUT(string fname) {
  ofstream file;
  file.open(fname, ios::out);
  if(file.is_open()){
    for(int n = 0;n < LUTs.size();n++){
      auto ptr = LUTs[n];
      for(int i = 0;i < LUT_RES; i++)
        for(int j = 0;j < 4;j++){
          file << ptr[i][j] << "\n";
      }
    }
  }
  file.close();
}
