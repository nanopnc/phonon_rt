#ifndef KEYVALUE
#define KEYVALUE

struct keyvalue {
  string key;
  double value;
};

#endif
