            ___      ___         ___         ___         ___         ___         ___          ___
           /  /\    /__/\       /  /\       /__/\       /  /\       /__/\       /  /\        /  /\   
          /  /::\   \  \:\     /  /::\      \  \:\     /  /::\      \  \:\     /  /::\      /  /:/  
         /  /:/\:\   \__\:\   /  /:/\:\      \  \:\   /  /:/\:\      \  \:\   /  /:/\:\    /  /:/  
        /  /:/~/:/__ /  /::\ /  /:/  \:\ _____\__\:\ /  /:/  \:\ _____\__\:\ /  /:/~/:/   /  /:/   
       /__/:/ /:/__/\  /:/\:/__/:/ \__\:/__/::::::::/__/:/ \__\:/__/::::::::/__/:/ /:/___/  /::\   
       \  \:\/:/\  \:\/:/__\\  \:\ /  /:\  \:\~~\~~\\  \:\ /  /:\  \:\~~\~~\\  \:\/:::::/__/:/\:\  
        \  \::/  \  \::/     \  \:\  /:/ \  \:\  ~~~ \  \:\  /:/ \  \:\  ~~~ \  \::/~~~~\__\/  \:\ 
         \  \:\   \  \:\      \  \:\/:/   \  \:\      \  \:\/:/   \  \:\      \  \:\         \  \:\
          \  \:\   \  \:\      \  \::/     \  \:\      \  \::/     \  \:\      \  \:\         \  \:\
           \__\/    \__\/       \__\/       \__\/       \__\/       \__\/       \__\/          \__\/   


phononRT is a lightweight software based on the Monte Carlo method for simulation of phonon propagation and transmission in 3D geometries. phononRT is being developed in the Nanoscience Center and the Department of Physics in the University of Jyväskylä.   

Example prompts:   

    mpirun -n 4 phononrt f=example.yml p=100000 d=res.dat sin_mfp=10e-9 t=trail.dat g=geom.dat   
    
**mpirun**         # launches a job in Open Run-Time Environment (ORTE)  
**-n 4**           # sets the number of parallel processes as 4   
**phononrt**       # name of the job launched   
**f=example.yml**  # reads the simulation configuration from file example.yml   
**p=100000**       # sets the number of phonons to 100000 for each process (total 400000)    
**d=res.dat**      # sets the results file name as res.dat [optional]    
**sin_mfp=10e-9**  # sets the variable 'sin_mfp' (declared in example.yml) to 10e-9 [optional]     
**t=trail.dat**    # exports the first particle trail in trail.dat [optional]    
**g=geom.dat**     # exports the geometry in file geom.dat [optional]    
