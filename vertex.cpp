#include "vertex.hpp"
#include <iostream>
#include <string>
#include <vector>
#include <cmath>

#define EPS 1e-15

vertex operator ^ (const vertex & a, const vertex & b) {
	// Cross product.
	vertex result;
	result.x = a.y * b.z - a.z * b.y;
	result.y = a.z * b.x - a.x * b.z;
	result.z = a.x * b.y - a.y * b.x;
	return(result);
	//return {a.y * b.z - a.z * b.y, a.z * b.x - a.x * b.z, a.x * b.y - a.y * b.x};
}

vertex operator - (const vertex & a, const vertex & b) {
	// Subtraction.
	vertex result;
	result.x = a.x - b.x;
	result.y = a.y - b.y;
	result.z = a.z - b.z;
	return(result);
	//return {a.x-b.x, a.y-b.y, a.z-b.z};
}

vertex operator + (const vertex & a, const vertex & b) {
	// Addition.
	vertex result;
	result.x = a.x + b.x;
	result.y = a.y + b.y;
	result.z = a.z + b.z;
	return(result);
	//return {a.x + b.x, a.y + b.y, a.z + b.z};
}

vertex operator * (const double & a, const vertex & b) {
	// Multiplication.
	vertex result;
	result.x = a * b.x;
	result.y = a * b.y;
	result.z = a * b.z;
	return(result);
	//return {a * b.x, a * b.y, a * b.z};
}

double dot (const vertex & a, const vertex & b) {
	// Dot product.
	return a.x * b.x + a.y * b.y + a.z * b.z;
}

void unit(vertex & v) {
	double vmod = sqrt(dot(v, v));
	if (vmod > EPS) {
		v.x /= vmod;
		v.y /= vmod;
		v.z /= vmod;
	}
}

ostream& operator<< (ostream& stream, const vertex& a){
	stream << "(" << a.x << ", " << a.y << ", " << a.z << ")";
	return stream;
}

vertex as_vertex(const string& s){
	// String syntax: [xx, yy, zz]
	vertex v;
	int bra1 = s.find("[");
	int bra2 = s.find("]");
	string sub = s.substr(bra1+1, bra2-bra1-1);

	int pos = sub.find(",");
	v.x = stod(sub.substr(0, pos));

	sub = sub.substr(pos + 1);
	pos = sub.find(",");
	v.y = stod(sub.substr(0, pos));

	sub = sub.substr(pos + 1);
	pos = sub.find(",");
	v.z = stod(sub.substr(0, pos));

	return v;
}

vertex as_vertex(const vector<double>& w){
	vertex v;
	v.x = w[0];
	v.y = w[1];
	v.z = w[2];
	return v;
}

vertex as_vertex(double x, double y, double z){
	vertex v;
	v.x = x;
	v.y = y;
	v.z = z;
	return v;
}
