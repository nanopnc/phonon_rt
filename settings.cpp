#include "settings.hpp"
#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include <fstream>
#include <stdexcept>
#include <cmath>
#include "vertex.hpp"
#include "keyvalue.hpp"

using namespace std;

const string WHITESPACE = " \n\r\t\f\v[],";

Settings::Settings(int argc, char** argv){

  vector<string> params;

  for(int i = 0;i < argc;i++){
    params.push_back(argv[i]);
  }

  for(int i = 1;i < argc;i++){
    string set = params[i];
    int pos = set.find("=");
    string token = set.substr(0, pos);
    string value = set.substr(pos + 1);
    switch(token.front()){
      case 'f':
      {
        filename = value;
        break;
      }
      case 'p':
      {
        nParticles = stoi(value);
        break;
      }
      case 'g':
      {
        geomFile = value;
        break;
      }
      case 'd':
      {
        dataFile = value;
        break;
      }
      case 'v':
      {
        verbose = stoi(value);
        break;
      }
      case 't':
      {
        trailFile = value;
        break;
      }
      case 'c':
      {
        collisionFile = value;
        break;
      }
      case 'l':
      {
        lutFile = value;
        break;
      }
      default: //define a variable
      {
        keyvalue var = {};
        var.key = token;
        var.value = stod(value);
        variables.push_back(var);
        break;
      }
    }
  }

  if(filename == ""){
    cout << "Error: model file not specified." << endl;
  } else {
    //Create default void material first
    material m;
    m.name = "Void"; //TODO: Give warning for unidentified material
    m.ct = 33e33;
    m.cl = 33e33;
    m.young = 33e33;
    m.poisson = 0;
    m.lambda = 33e33;
    m.mu = 33e33;
    m.density = 0;
    m.mfp = 33e33;
    materials.push_back(m);

    int mode;
    ifstream file(filename);
    if(file.is_open()){
      string line;
      while(getline(file, line)){

        // Skip comment lines
        if(ltrim(line).find("#") == 0) continue;

        // Extract a keyword
        int pos = line.find(":");
        if(pos != string::npos){
          string token = ltrim(line.substr(0, pos));

          // First materials block, then geometry block
          if(token == "materials"){
            mode = 0;
            continue;
          } else if(token == "geometry"){
            mode = 1;
            continue;
          } else if(token == "variables"){
            mode = 2;
            continue;
          }
          // Read value
          string value = rtrim(ltrim(line.substr(pos + 1)));

          // If empty, define a new material or a new geometry shape
          if(value ==  ""){
            if(mode == 0) {
              material m = {"", 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
              m.name = token;
              materials.push_back(m);
            } else if(mode == 1) {
              shape h;
              h.name = token;
              geometry.push_back(h);
            }
          } else if(mode == 0 | mode == 1) {
            if(token == "cl"){
              materials.back().cl = evaluateExpression(value);
            } else if(token == "ct"){
              materials.back().ct = evaluateExpression(value);
            } else if(token == "young"){
              materials.back().young = evaluateExpression(value);
            } else if(token == "poisson"){
              materials.back().poisson = evaluateExpression(value);
            } else if(token == "density"){
              materials.back().density = evaluateExpression(value);
            } else if(token == "mfp"){
              materials.back().mfp = evaluateExpression(value);
            } else if(token == "type"){
              geometry.back().type = value;
              if(value == "stl"){
                //need nSurfaces nVertices values
                //do that after reading the file

              } else if(value == "box"){
                nSurfaces += 6;
                nVertices += 30;
              } else if(value == "cylinder6"){
                nSurfaces += 8;
                nVertices += 44;
              } else if(value == "cylinder8"){
                nSurfaces += 10;
                nVertices += 58;
              } else if(value == "cylinder12"){
                nSurfaces += 14;
                nVertices += 86;
              } else if(value == "prism"){
                nSurfaces += 5;
                nVertices += 23;
              } else if(value == "inlet"){
                geometry.back().Xlet = 1;
              } else if(value == "outlet"){
                geometry.back().Xlet = -1;
                geometry.back().sink = true;
                nOutlets++;
              } else {
                cout << "Error: unknown geometry type in settings file!"
                  << endl;
                continue;
              }
            } else if(token == "filename"){
              geometry.back().filename = ltrim(rtrim(value));
              // Read
              if(geometry.back().type == "stl"){
                int nvert = 0;
                string line;
                string item;
                ifstream stlfile(geometry.back().filename);
                if(stlfile.is_open()){
                  while (getline(stlfile, line)){
                    int pos = line.find_first_of(" ");
                    item = line.substr(0, pos);
                    if(item == "vertex"){
                      nvert++;
                      vector<double> A = 
                        as_vectorDouble(line.substr(pos + 1));
                      for(int i = 0; i < 3; i++){
                        geometry.back().vertices.push_back(A[i]);
                      }                     
                    }
                  }
                  nSurfaces += nvert/3;
                  nVertices += nvert;
                } else {
                  cout << "Error: STL file couldn't be opened!" << endl;
                }
              } else {
                cout << "Error: Attribute filename does not relate to "
                  << "a supported format." << endl;
                continue;
              }
            } else if(token == "vertices"){
              geometry.back().vertices = as_vectorDouble(value);
              if(geometry.back().vertices.size() % 3 != 0){
                cout << "Error: Number of components in vertices must be" <<
                  " divisible by 3!" << endl;
                continue;
              }
            } else if(token == "corner"){
              geometry.back().corner = as_vertex(as_vectorDouble(value));
            } else if(token == "center"){
              geometry.back().center = as_vertex(as_vectorDouble(value));
            } else if(token == "dim"){
              geometry.back().dim = as_vertex(as_vectorDouble(value));
            } else if(token == "extrude"){
              geometry.back().extrude = evaluateExpression(value);
            } else if(token == "material"){
              for(int i = 0;i < materials.size();i++){
                if(value.compare(materials[i].name) == 0){
                  geometry.back().material = i;
                }
              }
            } else if(token == "face_specularity"){
              geometry.back().faceSpecularity = as_vectorDouble(value);
            } else if(token == "periodic"){
              geometry.back().periodicSurfaces = as_vectorInt(value);
              nPeriodicSurfaces += geometry.back().periodicSurfaces.size();
            } else if(token == "skip"){
              geometry.back().skip = as_vectorInt(value);
              nSurfaces -= geometry.back().skip.size();
            } else if(token == "periodic_position"){
              geometry.back().periodicPosition = as_vectorInt(value);
            } else if(token == "sink"){
              geometry.back().sink = as_bool(value);
              if(geometry.back().Xlet == 1)
                nOutlets++;
            }
          } else if(mode == 2){ //variables
            keyvalue var = {};
            var.key = token;
            var.value = stod(value);
            variables.push_back(var);
          }
        }
      }
    }
  }
  // Fill in all other material parameters
  for(auto m = begin(materials); m < end(materials); m++){
    if((*m).ct == 0.0 & (*m).cl == 0.0){
      if((*m).young == 0.0 | (*m).poisson == 0.0 | (*m).density == 0.0){
        cout << "Error: insufficient material parameters for material: "
          << (*m).name << endl;
        continue;
      }
      (*m).lambda = (*m).young*(*m).poisson/
        (1 + (*m).poisson)/(1 - 2*(*m).poisson);
      (*m).mu = (*m).young/2/(1 + (*m).poisson);
      (*m).cl = sqrt(((*m).lambda + 2*(*m).mu)/(*m).density);
      (*m).ct = sqrt((*m).mu/(*m).density);
    } else if((*m).young == 0.0 & (*m).poisson == 0.0){
      if((*m).ct == 0.0 | (*m).cl == 0.0){
        cout << "Error: insufficient material parameters for material: "
          << (*m).name << endl;
        continue;
      }
      (*m).lambda = (*m).density*(pow((*m).cl, 2) - 2*pow((*m).ct, 2));
      (*m).mu = (*m).density*pow((*m).ct, 2);
      (*m).young = (*m).mu*((*m).lambda + (*m).mu*2)/((*m).lambda + (*m).mu);
      (*m).poisson = (*m).lambda/2/((*m).lambda + (*m).mu);
    }
  }
}
/*
double Settings::readVariable(const string& s){
  for(keyvalue a : variables){
    if(a.key == s){
      return a.value;
    }
  }
  double value;
  try{
    size_t pos = s.find_last_of("*+-");
    if(pos == string::npos){
      value = stod(s);
    } else {
      double left = readVariable(ltrim(rtrim(s.substr(0, pos))));
      double right = readVariable(ltrim(rtrim(s.substr(pos + 1))));
      char op = s[pos];
      switch(op){
        case '+':
          value = left + right;
          break;
        case '*':
          value = left * right;
          break;
        case '-':
          value = left - right;
          break;
        default:
          cout << "Error: unknown operator " << op << endl;
      }
    }
  }
  catch (const invalid_argument& ia) {
    cout << "Error: unknown variable or value " << s << endl;
  }
  return value;
}
*/
bool Settings::is_verbose(){
  return verbose == 1;
}

void Settings::printSettings(){
  cout << "materials: " << endl;
  for(int i = 0;i < materials.size();i++){
    cout << "  " << materials[i].name << ":" << endl;
    cout << "    cl: " << materials[i].cl << endl;
    cout << "    ct: " << materials[i].ct << endl;
    cout << "    density: " << materials[i].density <<  endl;
    cout << "    young: " << materials[i].young << endl;
    cout << "    poisson: " << materials[i].poisson << endl;
    cout << "    lambda: " << materials[i].lambda << endl;
    cout << "    mu: " << materials[i].mu << endl;
    cout << "    mfp: " << materials[i].mfp << endl;
  }

  cout << "geometry: " << endl;
  for(int i = 0;i < geometry.size();i++){
    cout << "  " << geometry[i].name << ":" << endl;
    cout << "    type: " << geometry[i].type << endl;
    cout << "    corner: " << geometry[i].corner << endl;
    cout << "    dim: " << geometry[i].dim << endl;
    cout << "    material: " << geometry[i].material << endl;
    cout << endl << "    face_specularity: ";
    printVector(geometry[i].faceSpecularity);
    cout << endl;
    cout << "    skip: ";
    printVector(geometry[i].skip);
    cout << endl;
  }
}

void Settings::printVector(vector<double> v){
  cout << "[";
  for(int i; i < v.size();i++)
    cout << v[i] << " ";
  cout << "]";
}

void Settings::printVector(vector<int> v){
  cout << "[";
  for(int i; i < v.size();i++)
    cout << v[i] << " ";
  cout << "]" << endl;
}

bool Settings::as_bool(const string& s){
  if(ltrim(rtrim(s)).compare("true") == 0)
    return true;
  else
    return false;
}

vector<double> Settings::as_vectorDouble(const string& input){
    vector<double> values;
    string delimiter = ",";
    string valueStr;

    // Check if the values are enclosed in brackets []
    size_t startPos = input.find('[');
    size_t endPos = input.find(']');
    if (startPos != string::npos && endPos != string::npos && endPos > startPos) {
        // Extract the values within the brackets
        valueStr = input.substr(startPos + 1, endPos - startPos - 1);
    } else {
        // Values are not enclosed in brackets, use the whole string
        valueStr = input;
    }

    // Tokenize the value string
    istringstream iss(valueStr);
    string token;
    while (getline(iss, token, delimiter[0])) {
        // Convert the token to double and add it to the vector
        double value = evaluateExpression(ltrim(rtrim(token)));
        values.push_back(value);
    }

    return values;
}

// vector<double> Settings::as_vectorDouble(const string& s){
//   vector<double> result;
//   string sub, part;
//   double value;
// 	int bra1 = s.find("[");
// 	int bra2 = s.find("]");
//   sub = s.substr(bra1+1, bra2-bra1-1);
//   int not_done = 1;
//   while(not_done) {
//     int pos = sub.find_first_of(",");
//     if(pos == string::npos)
//         not_done = 0;
//     part = sub.substr(0, pos);
//     sub = sub.substr(pos + 1);
//     value = evaluateExpression(ltrim(rtrim(part)));
//     result.push_back(value);
//   }
//   return result;
// }

vector<int> Settings::as_vectorInt(const string& s){
  vector<int> result;
  string sub, part;
	int bra1 = s.find("[");
	int bra2 = s.find("]");
  sub = s.substr(bra1 + 1, bra2 - bra1 - 1);
  int not_done = 1;
  while(not_done) {
    int pos = sub.find(",");
    if(pos == string::npos)
      not_done = 0;

    part = sub.substr(0, pos);
    sub = sub.substr(pos + 1);
    result.push_back(stoi(part));
  }
  return result;
}

string Settings::ltrim(const string& s)
{
  size_t start = s.find_first_not_of(WHITESPACE);
  return (start == string::npos) ? "" : s.substr(start);
}

string Settings::rtrim(const string& s)
{
    size_t end = s.find_last_not_of(WHITESPACE);
    return (end == string::npos) ? "" : s.substr(0, end + 1);
}

int Settings::getTotalPeriodicSurfaces(){
  return nPeriodicSurfaces;
}

int Settings::getTotalSurfaces(){
  return nSurfaces;
}

int Settings::getTotalMaterials(){
  return materials.size();
}

int Settings::getTotalVertices(){
  return nVertices;
}

int Settings::getTotalOutlets(){
  return nOutlets;
}

int Settings::precedence(char op) {
    if (op == '+' || op == '-')
        return 1;
    else if (op == '*' || op == '/')
        return 2;
    else if (op == '^')
        return 3;
    else
        return 0;
}

double Settings::applyOp(double a, double b, char op) {
    switch (op) {
        case '+':
            return a + b;
        case '-':
            return a - b;
        case '*':
            return a * b;
        case '/':
            return a / b;
        case '^':
            return pow(a, b);
    }
    return 0;
}

double Settings::evaluateExpression(const string& expression) {
    vector<double> values;
    vector<char> ops;

    for (size_t i = 0; i < expression.length(); i++) {
        if (expression[i] == ' ')
            continue;
        else if (isdigit(expression[i]) || expression[i] == '.') {
            size_t j;
            double num = stod(expression.substr(i), &j);
            values.push_back(num);
            i += j - 1;
        }
        else if (isalpha(expression[i])) {
            string variableName;
            while (isalnum(expression[i]) || expression[i] == '_') {
                variableName += expression[i];
                i++;
            }
            i--;

            double variableValue = 0.0;
            bool variableFound = false;
            for (const auto& variable : variables) {
                if (variable.key == variableName) {
                    variableValue = variable.value;
                    variableFound = true;
                    break;
                }
            }

            if (variableFound)
                values.push_back(variableValue);
            else {
                cerr << "Variable '" << variableName << "' not found!" << endl;
                return 0.0;
            }
        }
        else if (expression[i] == '(')
            ops.push_back(expression[i]);
        else if (expression[i] == ')') {
            while (!ops.empty() && ops.back() != '(') {
                double b = values.back();
                values.pop_back();
                double a = values.back();
                values.pop_back();
                char op = ops.back();
                ops.pop_back();
                values.push_back(applyOp(a, b, op));
            }
            if (!ops.empty())
                ops.pop_back();
        }
        else if (expression[i] == '-' && (i == 0 || expression[i - 1] == '(')) {
            // Negative number handling
            size_t j;
            double num = stod(expression.substr(i), &j);
            values.push_back(num);
            i += j - 1;
        }
        else {
            while (!ops.empty() && precedence(ops.back()) >= precedence(expression[i])) {
                double b = values.back();
                values.pop_back();
                double a = values.back();
                values.pop_back();
                char op = ops.back();
                ops.pop_back();
                values.push_back(applyOp(a, b, op));
            }
            ops.push_back(expression[i]);
        }
    }

    while (!ops.empty()) {
        double b = values.back();
        values.pop_back();
        double a = values.back();
        values.pop_back();
        char op = ops.back();
        ops.pop_back();
        values.push_back(applyOp(a, b, op));
    }

    return values.back();
}