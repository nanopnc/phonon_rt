#include <iostream>
#include <fstream>
#include <cmath>
#include "particle.hpp"
#include "pcg32.h"

//#define DEBUG

using namespace std;

extern pcg32 rng;

void Particle::init() {
  // Initialize array for storing paths in materials
  if(totalPaths == NULL)
    totalPaths = new double[3*(model->nMaterials - 1)];
  for(int i = 0;i < 3*(model->nMaterials - 1); i++)
    totalPaths[i] = 0;

  // Find which inlet surface particle is initiated
  double inletVal = rng.nextDouble()*totalInletArea;
  for(int i = 0; i < model->inletPolys.size(); i++){
    if(inletVal < inletAreas[i]){
      inletSurface = i;
      break;
    }
  }
  int s = (model->inletPolys[inletSurface]).size();

  // Particle initialization inside a rectangle face
  vertex v0 = model->inletPolys[inletSurface][0];
  vertex o1 = model->inletPolys[inletSurface][1] - v0;
  vertex o2 = model->inletPolys[inletSurface][s - 2] - v0;

  double p1 = rng.nextDouble();
  double p2 = rng.nextDouble();
  if(s == 4){
    if(p1 + p2 > 1){
      // Uniform distribution in triangle
      position = v0 + (1 - p1)*o1 + (1 - p2)*o2;
    } else {
      position = v0 + p1*o1 + p2*o2;
    }
  } else {
    position = v0 + p1*o1 + p2*o2;
  }

  // Find surface normal of the initial surface
  surface = findSurface();
  model->checkOrientation(surface);
  vertex normal = model->surfaceNormals[surface];

  // Randomize angle
  double a1 = asin(sqrt(rng.nextDouble()));
  double a2 = 2*M_PI*rng.nextDouble();

  unit(o1);
  unit(o2);
  rotation = normal;

  direction = sin(a1)*(sin(a2)*o1 + cos(a2)*o2) + cos(a1)*normal;
  //unit(direction);


  // Set initial material
  initialMaterial = model->surfaceMaterials[surface];
  collision = 0;
  finalXlet = -1;

  // Randomize polarization
  double pol = rng.nextDouble();
  if(pol < 0.3333){ //P-wave
    polarization = direction;
    mode = 0;
    return;
  } else if(pol < 0.6666) { //SV-wave
    polarization = direction^(direction^normal);
    mode = 1;
    return;
  } else { //SH-wave
    polarization = direction^normal;
    mode = 2;
  }
}

void Particle::polarize() {
  // With respect to current surface where particle is located, modify
  // polarization so that S-wave is purely SH or SV
  if(mode != 0){
    vertex surf = direction^(model->surfaceNormals[surface]);

    vertex psh = dot(polarization, surf)*surf;
    double lpsh = sqrt(dot(psh, psh));

    if(rng.nextDouble() < lpsh){
      polarization = (1/lpsh)*psh;
      mode = 1;
    } else {
      polarization = polarization - psh;
      unit(polarization);
      mode = 2;
    }
  }
}

void Particle::rotate(vertex u, double angle) {
  double ct = cos(angle);
  double st = sin(angle);
  vertex n = model->surfaceNormals[surface];
  direction.x = (ct + pow(u.x, 2)*(1 - ct))*n.x +
    (u.x*u.y*(1 - ct) - u.z*st)*n.y + (u.x*u.z*(1 - ct) + u.y*st)*n.z;
  direction.y = (u.y*u.x*(1 - ct) + u.z*st)*n.x +
    (ct + pow(u.y, 2)*(1 - ct))*n.y + (u.y*u.z*(1 - ct) - u.x*st)*n.z;
  direction.z = (u.z*u.x*(1 - ct) - u.y*st)*n.x +
    (u.z*u.y*(1 - ct) + u.x*st)*n.y + (ct + pow(u.z, 2)*(1 - ct))*n.z;
#ifdef DEBUG
  cout << "Rotating around: " << u << " angle " << angle << endl;
  cout << "  result: " << direction << endl;
#endif
}

Particle::Particle(Model *m) {
  model = m;

  // Create vector of inlet areas for randomizing starting position
  totalInletArea = 0;
  for(int i = 0; i < model->inletPolys.size(); i++){
    double area = model->getArea(model->inletPolys[i]);
    totalInletArea += area;
    inletAreas.push_back(totalInletArea);
  }
}

int Particle::findSurface() {
  for(int i = 0;i < model->nSurfaces; i++) {
    if(model->isInside(position, i))
      return i;
  }
  return -1;
}

void Particle::exportFile(string fname) {
  ofstream file;
  file.open(fname, ios::app);
  if(file.is_open()){
    file << position << ", " << direction << ", " << surface << ", "
      << mode << ", " << polarization << ", " << rotation << endl;
  }
  file.close();
}

void Particle::printParticle() {
  cout << "Particle: " << endl;
  cout << "  pos: " << 1e9*position << " in [nm]" << endl;
  cout << "  dir: " << direction << endl;
  cout << "  surface: " << surface << endl;
  cout << "  mode: ";
  if(mode == 0)
    cout << "P";
  else if(mode == 1)
    cout << "SV";
  else // mode == 2
    cout << "SH";
  cout << endl << "  polarization: " << polarization << endl;
  cout << "  collisions: " << collision << endl;
}
